import React, { Component } from 'react'
import ReactDom from 'react-dom'

import './App.css'
import InputText from './components/InputText'
import TranslatedText from './components/TranslatedText'
import Settings from './components/Settings'
import History from './components/History'
import Languages from './components/Languages'
import Popup from './components/Popup'
import WordTranslation from './components/WordTranslation'

import { split } from './helpers/textHelper'
import storage from './helpers/storage'
import * as api from './helpers/yandexApi'
import selectWord from './helpers/selectionHelper'
import exampleText from './example'

const TRANSLATION_TIMEOUT = 2000
const INIT_CHUNK_SIZE = 300
const INIT_FONT_SIZE = 16

const initState = {
  text: '',
  originChunks: [],
  translatedChunks: [],
  historyVissible: false,
  languagesVissible: false,
  wordTranslation: { vissible: false },
  language: storage.getLanguage() || 'en',
  langsObject: {},
  detectedLanguage: '',
  chunkSize: storage.getChunkSize(INIT_CHUNK_SIZE),
  fontSize: storage.getFontSize(INIT_FONT_SIZE),
  nightMode: storage.getNightMode()
}

class App extends Component {
  constructor(...args) {
    super(...args)
    this.state = initState
    this.onTextChange = this.onTextChange.bind(this)
    this.translate = this.translate.bind(this)

    this.showHistory = this.toggleHistory.bind(this, true)
    this.closeHistory = this.toggleHistory.bind(this, false)
    this.setFromHistory = this.setFromHistory.bind(this)
    this.showLanguages = this.toggleLanguages.bind(this, true)
    this.closeLanguages = this.toggleLanguages.bind(this, false)
    this.setLanguage = this.setLanguage.bind(this)
    this.setFontSize = this.setFontSize.bind(this)
    this.setChunkSize = this.setChunkSize.bind(this)
    this.toggleNightMode = this.toggleNightMode.bind(this)
    this.translateWord = this.translateWord.bind(this)
    this.onClick = this.onClick.bind(this)
    this.updateBodyNightMode()
  }

  componentDidMount() {
    if (window.location.href.match(/\?example/)) {
      this.setState({ text: exampleText }, this.translate)
    }
    this.loadLanguages()
  }

  async loadLanguages() {
    const langsObject = await api.getLangs()
    this.setState({ langsObject })
  }

  onTextChange(text) {
    this.setState({ text })
    setTimeout(() => this.translate(), TRANSLATION_TIMEOUT)
  }

  async translate() {
    const { text, language, chunkSize } = this.state

    if (!text) {
      return
    }
    storage.addToHistory(text)

    const originChunks = split(text, chunkSize)
    this.setState({
      originChunks,
      translatedChunks: []
    })
    const translatePromises = originChunks.map(chunk =>
      api.translate(chunk, language)
    )

    const translations = await Promise.all(translatePromises)
    this.setState({
      translatedChunks: translations.map(x => x.text),
      detectedLanguage: translations[0] && translations[0].lang
    })
  }

  toggleHistory(historyVissible) {
    this.setState({ historyVissible })
  }

  async toggleLanguages(languagesVissible) {
    if (languagesVissible && !this.state.langsObject) {
      await this.loadLanguages()
    }
    this.setState({ languagesVissible })
  }

  setFromHistory(text) {
    this.setState({ text }, this.translate)
    this.closeHistory()
  }

  setLanguage(language) {
    this.setState({ language }, this.translate)
    this.closeLanguages()
    storage.setLanguage(language)
  }

  setFontSize(fontSize) {
    storage.setFontSize(fontSize)
    this.setState({ fontSize })
  }

  setChunkSize(chunkSize) {
    storage.setChunkSize(chunkSize)
    this.setState({ chunkSize }, this.translate)
  }

  toggleNightMode() {
    const nightMode = !this.state.nightMode
    this.setState({ nightMode }, this.updateBodyNightMode)
    storage.setNightMode(nightMode)
  }

  updateBodyNightMode() {
    document.body.classList.toggle('night', this.state.nightMode)
  }

  async translateWord() {
    const { detectedLanguage } = this.state
    const { word, coords } = selectWord()
    const translation = await api.lookup(word, detectedLanguage)
    this.setState({
      wordTranslation: {
        vissible: !!translation.length,
        word,
        coords,
        translation
      }
    })
  }

  onClick(e) {
    const popupDom = this.wordPopup && ReactDom.findDOMNode(this.wordPopup)
    if (popupDom && !popupDom.contains(e.target)) {
      this.setState({ wordTranslation: { vissible: false } })
    }
  }

  render() {
    const {
      originChunks,
      translatedChunks,
      historyVissible,
      languagesVissible,
      wordTranslation,
      langsObject,
      language,
      detectedLanguage,
      text,
      fontSize,
      chunkSize,
      nightMode
    } = this.state
    const pinnedLangs = storage.getLanguages()
    const currentLang = langsObject[language]
    let detectedName = ''
    if (detectedLanguage) {
      const [langKey] = detectedLanguage.split('-')
      detectedName = langsObject[langKey]
    }

    return (
      <div className={`app ${nightMode ? 'night' : ''}`} onClick={this.onClick}>
        <div className="app-header">
          <h2>Translate your story</h2>
        </div>
        <div className="main">
          <div>
            <InputText text={text} onChange={this.onTextChange} />
            {detectedName &&
              <div className="detected">
                Detected language: {detectedName}
              </div>}
            {currentLang &&
              <div className="current-language">
                {`Translate to: ${currentLang}`}
              </div>}
          </div>
          <div className="main-buttons">
            <button className="btn" onClick={this.translate}>
              Translate
            </button>
            <button className="btn" onClick={this.showHistory}>
              History
            </button>
            <button className="btn" onClick={this.showLanguages}>
              My Language
            </button>
          </div>
          <Settings
            fontSize={fontSize}
            chunkSize={chunkSize}
            setFontSize={this.setFontSize}
            setChunkSize={this.setChunkSize}
            toggleNightMode={this.toggleNightMode}
            nightMode={nightMode}
          />
          <TranslatedText
            originChunks={originChunks}
            translatedChunks={translatedChunks}
            fontSize={fontSize}
            onOriginClick={this.translateWord}
            wordTranslation={wordTranslation}
          />
        </div>
        {historyVissible &&
          <Popup closePopup={this.closeHistory}>
            <History setFromHistory={this.setFromHistory} />
          </Popup>}
        {languagesVissible &&
          <Popup closePopup={this.closeLanguages}>
            <Languages
              setLanguage={this.setLanguage}
              langsObject={langsObject}
              pinnedLangs={pinnedLangs}
              nightMode={nightMode}
            />
          </Popup>}
        {wordTranslation.vissible &&
          <WordTranslation
            ref={x => (this.wordPopup = x)}
            wordTranslation={wordTranslation}
          />}
      </div>
    )
  }
}

export default App
