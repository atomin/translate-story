// docs:
// translate: https://tech.yandex.com/translate/doc/dg/reference/translate-docpage/
// dictionary: https://tech.yandex.ru/dictionary/doc/dg/reference/lookup-docpage/

const TRANSLATE_URL = 'https://translate.yandex.net/api/v1.5/tr.json'
// const DICTIONARY_URL = 'https://dictionary.yandex.net/api/v1/dicservice.json/lookup'
const DICTIONARY_URL =
  'https://dictionary.yandex.net/dicservice.json/lookupMultiple?ui=en&srv=tr-text&sid=7cb0e8a0.597e49b9.d686d27c&'

// https://translate.yandex.net/api/v1.5/tr.json/translate ?
// key=<API key>
//  & text=<text to translate>
//  & lang=<translation direction>
//  & [format=<text format>]
//  & [options=<translation options>]
//  & [callback=<name of the callback function>]

// lang can be destination language like 'en' or pair source-destianation like 'es-en'
export async function translate(text, lang) {
  // REACT_APP_YANDEX_KEY must be in env vars
  const url = encodeURI(
    `${TRANSLATE_URL}/translate?key=${process.env
      .REACT_APP_YANDEX_KEY}&text=${text}&lang=${lang}`
  )
  const res = await fetch(url, { mode: 'cors' })
  const json = await res.json()
  return {
    text: json.text,
    lang: json.lang
  }
}

// https://translate.yandex.net/api/v1.5/tr.json/getLangs ?
// key=<API key>
//  & [ui=<language code>]
//  & [callback=<name of the callback function>]

export async function getLangs() {
  // REACT_APP_YANDEX_KEY must be in env vars
  const url = encodeURI(
    `${TRANSLATE_URL}/getLangs?key=${process.env.REACT_APP_YANDEX_KEY}&ui=en`
  )
  try {
    const res = await fetch(url, { mode: 'cors' })
    const json = await res.json()
    return json.langs
  } catch (e) {
    return []
  }
}

// https://dictionary.yandex.net/api/v1/dicservice.json/lookup?key=API-ключ&lang=en-ru&text=time

// lang can be destination language like 'en' or pair source-destianation like 'es-en'
export async function lookup1(text, lang) {
  // REACT_APP_YANDEX_KEY must be in env vars
  const url = encodeURI(
    `${DICTIONARY_URL}?key=${process.env
      .REACT_APP_YANDEX_DICTIONARY_KEY}&lang=${lang}&text=${text}`
  )
  const res = await fetch(url, { mode: 'cors' })
  const json = await res.json()
  return json
}

// lang can be destination language like 'en' or pair source-destianation like 'es-en'
export async function lookup(text, lang) {
  // REACT_APP_YANDEX_KEY must be in env vars
  const url = encodeURI(`${DICTIONARY_URL}text=${text}&dict=${lang}&flags=103`)
  const res = await fetch(url, { mode: 'cors' })
  const json = await res.json()
  return json[lang].regular
}
