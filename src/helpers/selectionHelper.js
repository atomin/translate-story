function getSelectionCoords(win) {
  win = win || window
  const doc = win.document
  let sel = doc.selection
  let range, rects, rect
  let x = 0
  let y = 0
  if (sel) {
    if (sel.type !== 'Control') {
      range = sel.createRange()
      range.collapse(true)
      x = range.boundingLeft
      y = range.boundingTop
    }
  } else {
    sel = win.getSelection && win.getSelection()
    if (sel && sel.rangeCount) {
      range = sel.getRangeAt(0).cloneRange()
      if (range.getClientRects) {
        range.collapse(true)
        rects = range.getClientRects()
        if (rects.length > 0) {
          rect = rects[0]
        }
        if (rect) {
          x = rect.left
          y = rect.top
        }
      }
      // Fall back to inserting a temporary element
      if (x === 0 && y === 0) {
        const span = doc.createElement('span')
        if (span.getClientRects) {
          // Ensure span has dimensions and position by
          // adding a zero-width space character
          span.appendChild(doc.createTextNode('\u200b'))
          range.insertNode(span)
          rect = span.getClientRects()[0]
          x = rect.left
          y = rect.top
          const spanParent = span.parentNode
          spanParent.removeChild(span)

          // Glue any broken text nodes back together
          spanParent.normalize()
        }
      }
    }
  }
  return { x, y }
}

const NON_WORD_REGEXP = /[ \n\r\t.,\\'"+!?-]+/

export default function selectWord(e) {
  const selection = window.getSelection()
  const range = selection.getRangeAt(0)
  const node = selection.anchorNode

  while (!range.toString().match(NON_WORD_REGEXP) && range.startOffset > 0) {
    range.setStart(node, range.startOffset - 1)
  }
  if (range.toString().match(NON_WORD_REGEXP)) {
    range.setStart(node, range.startOffset + 1)
  }
  while (
    !range.toString().match(NON_WORD_REGEXP) &&
    range.toString().trim() !== '' &&
    range.endOffset < node.length
  ) {
    range.setEnd(node, range.endOffset + 1)
  }
  if (range.toString().match(NON_WORD_REGEXP)) {
    range.setEnd(node, range.endOffset - 1)
  }
  // selection.removeAllRanges()
  const word = range.toString().trim()
  return {
    word,
    coords: getSelectionCoords()
  }
}
