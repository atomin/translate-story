import { split } from './textHelper'
import assert from 'assert'

const testText = `
Capítulo!
Dorothy vivía en las grandes praderas de Kansas con
su tío Henry y la tía Em – su mujer!
Ellos eran granjeros y vivían en una casa muy pequeña.
La casa tenía sólo una habitación y no había muchas cosas en ella. 
En la casa había una mesa, un armario, tres o cuatro sillas y dos camas.
La única ventana daba a un pequeño jardín. 
Pero cuando Dorothy miraba por la ventana no veía ni árboles ni flores cerca de la casa. 
Ni siquiera la hierba era verde sino gris. 
Todo en sus vidas era de color gris, todo era muy triste.
El tío Henry y la tía Em trabajaban todo el día. Empezaban muy temprano por la mañana, paraban tarde por la noche y nunca se reían.
A pesar de esto, Dorothy era feliz porque tenía un amigo.
Su amigo era un perro de color negro llamado Totó. 
Él tenía pelo largo, ojos brillantes y cola corta.
El perro y la niña corrían y jugaban todos los días. 
Eran los mejores amigos y se entendían uno al otro sin palabras.
`

describe('textHelper', () => {
  it('shoule split empty string', () => {
    assert.deepEqual(split(''), [''])
  })

  it('shoule split without params', () => {
    assert.deepEqual(split(), [''])
  })

  it('shoule split text by sentenses in case of small chunks', () => {
    const splited = split(testText, 1)
    assert.equal(splited[0], 'Capítulo!')
    assert.equal(
      splited[1],
      `Dorothy vivía en las grandes praderas de Kansas con
su tío Henry y la tía Em – su mujer!`
    )
    assert.equal(
      splited[15],
      'Eran los mejores amigos y se entendían uno al otro sin palabras.'
    )
    assert.equal(splited.length, 16)
  })

  it('shoule split text by chunks', () => {
    const splited = split(testText, 100)
    assert.equal(
      splited[0],
      `Capítulo!Dorothy vivía en las grandes praderas de Kansas con
su tío Henry y la tía Em – su mujer!`
    )
    assert.equal(
      splited[1],
      `Ellos eran granjeros y vivían en una casa muy pequeña.La casa tenía sólo una habitación y no había muchas cosas en ella.`
    )
    assert.equal(splited.length, 9)
  })
})
