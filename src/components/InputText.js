import React, { Component } from 'react'

import './InputText.css'

const INPUT_HEIGHT_TIMEOUT = 2000
const MAX_HEIGHT = 200
const MIN_HEIGHT = 50

const initState = {
  inputHeight: MAX_HEIGHT
}

export default class InputText extends Component {
  constructor(...args) {
    super(...args)
    this.onChange = this.onChange.bind(this)
    this.onFocus = this.onFocus.bind(this)
    this.onBlur = this.onBlur.bind(this)
    this.clear = this.clear.bind(this)
    this.state = initState
  }

  componentDidMount() {
    if (window.location.href.match(/\?example/)) {
      this.setHeightTimer(true)
    }
  }

  onChange(e) {
    this.setText(e.target.value)
  }

  setText(value) {
    this.clearHeightTimer()
    this.setState({ inputHeight: MAX_HEIGHT })
    this.props.onChange(value)
  }

  clear() {
    this.setText('')
  }

  heightDecrease() {
    this.setState({
      inputHeight: MIN_HEIGHT
    })
  }

  onFocus() {
    this.clearHeightTimer()
    this.setState({
      inputHeight: MAX_HEIGHT
    })
  }

  onBlur() {
    this.setHeightTimer()
  }

  clearHeightTimer() {
    if (this.heightTimer) {
      clearTimeout(this.heightTimer)
      this.heightTimer = null
    }
  }

  setHeightTimer(force) {
    this.clearHeightTimer()
    if (this.props.text.length || force) {
      this.heightTimer = setTimeout(
        () => this.heightDecrease(),
        INPUT_HEIGHT_TIMEOUT
      )
    }
  }

  render() {
    const { text } = this.props
    const { inputHeight } = this.state

    return (
      <div className="text-input-wrapper">
        <textarea
          type="text"
          className="text-input"
          style={{ height: inputHeight }}
          value={text}
          onChange={this.onChange}
          onFocus={this.onFocus}
          onBlur={this.onBlur}
          placeholder="place your story..."
        />
        <button className="btn clear-button" onClick={this.clear}>
          Clear
        </button>
      </div>
    )
  }
}
