import React from 'react'

import './Languages.css'
import pinImageDay from '../images/pin.png'
import pinImageNight from '../images/pin_night.png'

function formLangsList(langsObject, pinnedLangsKeys) {
  const pinnedLangs = pinnedLangsKeys
    .filter(key => key in langsObject)
    .map(key => ({ key: key, name: langsObject[key], pinned: true }))
  const alpabeticLangs = Object.entries(langsObject)
    .map(([key, name]) => ({ key, name }))
    .filter(({ key, name }) => !pinnedLangsKeys.includes(key))
  return [...pinnedLangs, ...alpabeticLangs]
}

export default function Languages({
  setLanguage,
  langsObject,
  pinnedLangs,
  nightMode
}) {
  const languages = formLangsList(langsObject, pinnedLangs)
  const pinImage = nightMode ? pinImageNight : pinImageDay
  return (
    <div className="languages">
      <div className="language-items">
        {languages.map((item, ind) =>
          <div
            key={ind}
            className="language-item"
            onClick={() => setLanguage(item.key)}
          >
            {item.name}
            {item.pinned &&
              <img
                className="pin-image"
                src={pinImage}
                alt="pinned language"
              />}
          </div>
        )}
        {!languages.length &&
          <div className="languages-error">
            Sorry, can't load languages now
          </div>}
      </div>
    </div>
  )
}
