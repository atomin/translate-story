import React from 'react'
import { HotKeys } from 'react-hotkeys'

import './Popup.css'

export default function Popup(props) {
  const { closePopup, children } = props

  function click(e) {
    if (e.currentTarget === e.target) {
      // This means user clicked to the gray background area, not inside the overlay children
      closePopup()
    }
  }

  const keyMap = {
    closePopup: ['esc']
  }
  const handlers = {
    closePopup
  }
  return (
    <div className="popup-background" onClick={click}>
      {children}
      <HotKeys keyMap={keyMap} handlers={handlers} focused attach={window} />
    </div>
  )
}
