import React from 'react'

import './TranslatedText.css'

function getOriginChunk(chunk, { word, vissible }) {
  if (!vissible) {
    return chunk
  }
  const parts = chunk
    .split(
      new RegExp(`(?:[ \n\r\t.,\\'"+!?-]+|^)${word}(?=[ \n\r\t.,\\'"+!?-]+|$)`)
    )
    .map((x, ind) => [
      <span key={ind}>
        {x}
      </span>,
      <span key={`${ind}-selected`} className="selected-word">
        {word}
      </span>
    ])

  const flatten = [].concat.apply([], parts)
  flatten.length--

  return flatten
}

function TranslatedText(props) {
  const {
    originChunks,
    translatedChunks,
    fontSize,
    onOriginClick,
    wordTranslation
  } = props
  return (
    <article className="translated-text">
      {originChunks.map((chunk, ind) =>
        <div key={`${ind}-chunk`} className="chunks-pair" style={{ fontSize }}>
          <p className="chunk origin-chunk" onClick={onOriginClick}>
            {getOriginChunk(chunk, wordTranslation)}
          </p>
          <p className="chunk translated-chunk">
            {translatedChunks[ind]}
          </p>
        </div>
      )}
      {!!translatedChunks.length &&
        <div className="yandex">
          <a href="http://translate.yandex.com/" target="about:blank">
            Powered by Yandex.Translate
          </a>
        </div>}
    </article>
  )
}

export default TranslatedText
