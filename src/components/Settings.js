import React from 'react'

import './Settings.css'
import paragraph from '../images/paragraph.svg'
import paragraph_night from '../images/paragraph_night.svg'
import paragraph_small from '../images/paragraph_small.svg'
import paragraph_small_night from '../images/paragraph_small_night.svg'

const CHUNK_SIZE_STEP = 50
const FONT_SIZE_RATE_FACTOR = 1.05

function Settings(props) {
  const {
    fontSize,
    chunkSize,
    setFontSize,
    setChunkSize,
    toggleNightMode,
    nightMode
  } = props

  const increaseFont = () => setFontSize(fontSize * FONT_SIZE_RATE_FACTOR)
  const decreaseFont = () => setFontSize(fontSize / FONT_SIZE_RATE_FACTOR)

  const increaseChunks = () => setChunkSize(chunkSize + CHUNK_SIZE_STEP)
  const decreaseChunks = () => setChunkSize(chunkSize - CHUNK_SIZE_STEP)

  return (
    <div className="settings">
      <button className="btn" onClick={decreaseFont}>
        <span className="decrease-font">A↓</span>
      </button>
      <button className="btn" onClick={increaseFont}>
        <span className="increase-font">A↑</span>
      </button>
      <button className="btn" onClick={decreaseChunks}>
        <img
          src={nightMode ? paragraph_small_night : paragraph_small}
          alt="paragraph"
          className="paragraph-image paragraph-small-image"
        />
      </button>
      <button className="btn" onClick={increaseChunks}>
        <img
          src={nightMode ? paragraph_night : paragraph}
          alt="paragraph"
          className="paragraph-image paragraph-big-image"
        />
      </button>
      <button className="btn night-day" onClick={toggleNightMode}>
        <span className={nightMode ? 'day-btn' : 'night-btn'}>
          {nightMode ? '✺' : '☽'}
        </span>
      </button>
    </div>
  )
}

export default Settings
